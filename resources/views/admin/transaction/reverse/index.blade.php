@extends('layouts.master')
@section('title')
    Pengembalian
@endsection
@section('content')

    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Pengembalian</h3>
            </div>
        </div>

        <div class="clearfix"></div>


        <div class="row">
            <div class="col-md-12 col-sm-12 ">
            </div>
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>DATA PENGEMBALIAN</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                    <table id="datatable-buttons" class="table table-striped table-bordered"
                                        style="width:100%">
                                        <thead>
                                            <tr>
                                                <th scope="col">No.</th>
                                                <th scope="col">Tgl Pengembalian</th>
                                                <th scope="col">Nama Peminjam</th>
                                                <th scope="col">Jenis Barang</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($reverses as $key => $reverse)
                                                <tr>
                                                    <th scope="row">{{ $loop->iteration }}</th>
                                                    <th scope="row">{{ $reverse->created_at }}</th>
                                                    <th scope="row">{{ $reverse->borrow->client->name }}</th>
                                                    <th scope="row">{{ $reverse->materials }}</th>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
