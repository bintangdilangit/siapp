@extends('layouts.master')
@section('title')
    Detail Pengembalian
@endsection
@section('content')
    <!-- page content -->

    <div class="">
        <div class="page-title">
            <div class="title_left mb-3">
                <label for="">Peminjam : </label>
                <h3></h3>
                {{-- @if ($borrow->client->role_id == 1)
                    <h2>Dari Mahasiswa</h2>
                @elseif ($borrow->client->role_id == 2)
                    <h2>Dari Tenaga Kependidikan</h2>
                @else
                    <h2>Dari Instansi Lain</h2>
                @endif --}}
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <!-- form input mask -->
            <div class="col">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Detail Pengembalian</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form class="form-horizontal form-label-left" action="{{ route('reverse.update', $reverse->id) }}"
                            method="post" novalidate>
                            @method('PUT')
                            @csrf

                            <div class="field item form-group after-add-more">
                                <label class="control-label col-md-3 col-sm-3 col-xs-3">Material<span
                                        class="required">*</span></label>
                                <div class="col-md-3 col-sm-1">
                                    {{-- <input type="text" name="material" id="" disabled value="@foreach ($return->materials as $material) {{ $material->name . '(Stock: ' . $material->stock . ')' }} @endforeach"> --}}
                                    {{-- <select class="select2-single form-control " name="material" id="id">
                                        @foreach ($borrow->materials as $material)
                                            <option value="{{ $material->id }}">
                                                {{ $material->name . '(Stock: ' . $material->stock . ')' }}</option>
                                        @endforeach
                                    </select> --}}
                                </div>
                                <label class="col-form-label col-md-1 col-sm-2 label-align">Jumlah<span
                                        class="required">*</span></label>
                                <div class="col-md-2 col-sm-1">
                                    <input class="form-control" name="returnAmount" required="required" value="0" />
                                </div>
                                <button class="btn btn-success add-more" type="button">
                                    <i class="glyphicon glyphicon-plus"></i> Add
                                </button>
                            </div>
                            {{-- <div class="copy invisible">
                                <div class="field item form-group control-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-3">Material<span
                                            class="required">*</span></label>
                                    <div class="col-md-3 col-sm-1">
                                        <input type="text" name="material" id="" disabled value="@foreach ($borrow->materials as $material) {{ $material->name . '(Stock: ' . $material->stock . ')' }} @endforeach">
                                    </div>
                                    <label class="col-form-label col-md-1 col-sm-2 label-align">Jumlah<span
                                            class="required">*</span></label>
                                    <div class="col-md-2 col-sm-1">
                                        <input class="form-control" name="borrowAmount" required="required"
                                            value="{{ $borrow->borrowAmount }}" />
                                    </div>
                                    <button class="btn btn-danger remove" type="button"><i
                                            class="glyphicon glyphicon-remove"></i> Remove</button>
                                </div>
                            </div> --}}
                            <div class="ln_solid"></div>

                            <div class="form-group row">
                                <div class="col-md-9 offset-md-3">
                                    <a class="btn btn-danger" href="{{ route('reverse.index') }}">Kembali</a>
                                    <button type="submit" class="btn btn-success">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /form input mask -->
        </div>
    </div>

@endsection
