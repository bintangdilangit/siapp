<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title></title>
    <script src="http://code.jquery.com/jquery-2.0.3.min.js" data-semver="2.0.3" data-require="jquery"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/datatables/1.9.4/css/jquery.dataTables_themeroller.css"
        rel="stylesheet" data-semver="1.9.4" data-require="datatables@*" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/datatables/1.9.4/css/jquery.dataTables.css" rel="stylesheet"
        data-semver="1.9.4" data-require="datatables@*" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/datatables/1.9.4/css/demo_table_jui.css" rel="stylesheet"
        data-semver="1.9.4" data-require="datatables@*" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/datatables/1.9.4/css/demo_table.css" rel="stylesheet"
        data-semver="1.9.4" data-require="datatables@*" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/datatables/1.9.4/css/demo_page.css" rel="stylesheet"
        data-semver="1.9.4" data-require="datatables@*" />
    <link data-require="jqueryui@*" data-semver="1.10.0" rel="stylesheet"
        href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.10.0/css/smoothness/jquery-ui-1.10.0.custom.min.css" />
    <script data-require="jqueryui@*" data-semver="1.10.0"
        src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.10.0/jquery-ui.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/datatables/1.9.4/jquery.dataTables.js" data-semver="1.9.4"
        data-require="datatables@*"></script>




    <!-- buttons -->
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css"
        href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>




    <!-- Bootstrap -->
    <link href="{{ asset('../../../../design/admin/vendors/bootstrap/dist/css/bootstrap.min.css') }} "
        rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('../../../../design/admin/vendors/font-awesome/css/font-awesome.min.css') }} "
        rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('../../../../design/admin/vendors/nprogress/nprogress.css') }} " rel="stylesheet">
    <!-- jQuery custom content scroller -->

    <link href="{{ asset('../../../../design/admin/build/css/custom.min.css') }}" rel="stylesheet">


    <style>
        h1 {
            color: red;
        }

    </style>
</head>

<body>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col menu_fixed">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="index.html" class="site_title"><i class="fa fa-stethoscope"></i> <span>Sistem
                                    Informasi
                                    Laboratorium</span></a>
                        </div>

                        <div class="clearfix"></div>

                        <!-- menu profile quick info -->
                        <div class="profile clearfix">
                            <div class="profile_pic">
                                <img src="{{ asset('../../../design/admin/production/images/img.jpg') }}" alt="..."
                                    class="img-circle profile_img">
                            </div>
                            <div class="profile_info">
                                <span>Welcome,</span>
                                <h2>{{ Auth::user()->name }}</h2>
                            </div>
                        </div>
                        <!-- /menu profile quick info -->

                        <br />

                        <!-- sidebar menu -->
                        @include('layouts.module.sidebar')
                        <!-- /sidebar menu -->

                        <!-- /menu footer buttons -->

                        <!-- /menu footer buttons -->
                    </div>
                </div>

                <!-- top navigation -->
                @include('layouts.module.navbar')
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">
                        <div class="page-title">
                            <div class="title_left">
                                <h3>Peminjaman</h3>
                            </div>
                            <div class="title_right mb-3">
                                <div class="col-sm-12 mt-3 text-right">
                                    <a href="{{ route('borrow.create') }}" type="button"
                                        class="btn btn-success">Tambah Peminjaman</a>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>


                        <div class="row">
                            <div class="col-md-12 col-sm-12 ">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>DATA PEMINJAMAN</h2>

                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <h2 style="font-size: 0.7rem">Date Format : BULAN/TANGGAL/TAHUN</h2>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <p id="date_filter">
                                                    <span id="date-label-from" class="date-label">From: </span><input
                                                        class="date_range_filter date" type="text"
                                                        id="datepicker_from" />
                                                    <span id="date-label-to" class="date-label">To:<input
                                                            class="date_range_filter date" type="text"
                                                            id="datepicker_to" />
                                                </p>
                                                <table width="100%" class="display table table-striped table-bordered"
                                                    id="datatable" class="table table-striped table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">No</th>
                                                            <th scope="col">Tanggal Peminjaman</th>
                                                            <th scope="col">No Identitas</th>
                                                            <th scope="col">Nama Peminjam</th>
                                                            <th scope="col">Nama Barang</th>
                                                            <th scope="col">Status</th>
                                                            <th scope="col"> Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($borrows as $key => $borrow)
                                                            <tr>
                                                                <th scope="row">{{ $loop->iteration }}</th>
                                                                <td scope="row">
                                                                    {{ date('m/d/Y', strtotime($borrow->created_at)) }}
                                                                </td>
                                                                <td scope="row">{{ $borrow->client->noId }}</td>
                                                                <td scope="row">{{ $borrow->client->name }}</td>


                                                                <td scope="row">
                                                                    @foreach ($borrow->materials as $material)
                                                                        {{ $material->name . '(' . $material->pivot->borrowAmount . ')' }}
                                                                        <br>
                                                                    @endforeach
                                                                </td>

                                                                <td scope="row">
                                                                    @if ($borrow->status == 1)
                                                                        Masih dipinjam
                                                                    @else
                                                                        Sudah dikembalikan
                                                                    @endif
                                                                </td>
                                                                <th scope="row">
                                                                    <!-- Button trigger modal Destroy -->
                                                                    <a href="{{ route('borrow.destroy', ['borrow' => $borrow]) }}"
                                                                        type="button" class="btn btn-danger"
                                                                        data-toggle="modal"
                                                                        data-target="#deleteConf{{ $borrow->id }}"><i
                                                                            class="fa fa-trash"></i>
                                                                        Delete
                                                                    </a>

                                                                    <!-- Modal -->
                                                                    <div class="modal fade"
                                                                        id="deleteConf{{ $borrow->id }}"
                                                                        tabindex="-1" role="dialog"
                                                                        aria-labelledby="deleteConfLabel"
                                                                        aria-hidden="true">
                                                                        <div class="modal-dialog" role="document">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <h5 class="modal-title"
                                                                                        id="deleteConfLabel">
                                                                                        Hapus Data Peminjaman</h5>
                                                                                    <button type="button" class="close"
                                                                                        data-dismiss="modal"
                                                                                        aria-label="Close">
                                                                                        <span
                                                                                            aria-hidden="true">x</span>
                                                                                    </button>
                                                                                </div>
                                                                                <div class="modal-body">
                                                                                    Yakin?
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <button class="btn btn-link"
                                                                                        data-dismiss="modal">
                                                                                        Cancel
                                                                                    </button>
                                                                                    <form
                                                                                        action="{{ route('borrow.destroy', ['borrow' => $borrow]) }}"
                                                                                        method="POST">
                                                                                        @csrf
                                                                                        @method('delete')
                                                                                        <button type="submit"
                                                                                            class="btn btn-success">Iya</button>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <br>

                                                                    <a href="{{ route('borrow.show', ['borrow' => $borrow]) }}"
                                                                        class="btn btn-info btn-xs"><i
                                                                            class="fa fa-info-circle"></i>
                                                                        Detail
                                                                    </a>

                                                                    <br>
                                                                    <!-- Button trigger modal Approve -->
                                                                    <a href="{{ route('approve', ['borrow' => $borrow]) }}"
                                                                        type="button"
                                                                        class="btn btn-success {{ $borrow->status == 0 ? 'disabled' : '' }}"
                                                                        data-toggle="modal"
                                                                        data-target="#approveConf{{ $borrow->id }}"><i
                                                                            class="fa fa-thumbs-o-up "></i>
                                                                        Approve
                                                                    </a>

                                                                    <!-- Modal -->
                                                                    <div class="modal fade"
                                                                        id="approveConf{{ $borrow->id }}"
                                                                        tabindex="-1" role="dialog"
                                                                        aria-labelledby="approveConfLabel"
                                                                        aria-hidden="true">
                                                                        <div class="modal-dialog" role="document">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <h5 class="modal-title"
                                                                                        id="approveConfLabel">
                                                                                        Pengembalian</h5>
                                                                                    <button type="button" class="close"
                                                                                        data-dismiss="modal"
                                                                                        aria-label="Close">
                                                                                        <span
                                                                                            aria-hidden="true">x</span>
                                                                                    </button>
                                                                                </div>
                                                                                <div class="modal-body">
                                                                                    Yakin?
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <button class="btn btn-link"
                                                                                        data-dismiss="modal">
                                                                                        Cancel
                                                                                    </button>
                                                                                    <form
                                                                                        action="{{ route('approve', ['borrow' => $borrow]) }}"
                                                                                        method="POST">
                                                                                        @csrf
                                                                                        <button type="submit"
                                                                                            class="btn btn-success">Iya</button>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </th>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </body>

</body>

<script>
    $(function() {

        $("#datepicker_from").datepicker({
            format: 'dd-mm-yyyy',
            showOn: "button",
            buttonImage: "",
            buttonImageOnly: false,

            "onSelect": function(date) {
                minDateFilter = new Date(date).getTime();
                oTable.fnDraw();
            }
        }).keyup(function() {
            minDateFilter = new Date(this.value).getTime();
            oTable.fnDraw();
        });

        $("#datepicker_to").datepicker({
            format: 'dd-mm-yyyy',
            showOn: "button",
            buttonImage: "",
            buttonImageOnly: false,
            "onSelect": function(date) {
                maxDateFilter = new Date(date).getTime();
                oTable.fnDraw();
            }
        }).keyup(function() {
            maxDateFilter = new Date(this.value).getTime();
            oTable.fnDraw();
        });
        var oTable = $('#datatable').DataTable({
            dom: 'Bfrtip',
            "buttons": [
                {
                    extend: 'copyHtml5',
                    title: 'Rekap Peminjaman'
                },
                {
                    extend: 'excelHtml5',
                    title: 'Rekap Peminjaman'
                },
                {
                    extend: 'pdfHtml5',
                    title: 'Rekap Peminjaman'
                },
                {
                    extend: 'csvHtml5',
                    title: 'Rekap Peminjaman'
                },
                {
                    extend: 'print',
                    title: 'Rekap Peminjaman'
                },
            ],
            "oLanguage": {
                "sSearch": "Search<br>"
            },
            "iDisplayLength": -1,
            "sPaginationType": "full_numbers",

        });

    });


    // Date range filter
    minDateFilter = "";
    maxDateFilter = "";


    $.fn.dataTableExt.afnFiltering.push(
        function(oSettings, aData, iDataIndex) {
            if (typeof aData._date == 'undefined') {
                aData._date = new Date(aData[1]).getTime();
            }

            if (minDateFilter && !isNaN(minDateFilter)) {
                console.log(minDateFilter && !isNaN(minDateFilter));
                if (aData._date < minDateFilter) {
                    return false;
                }
            }

            if (maxDateFilter && !isNaN(maxDateFilter)) {
                console.log(maxDateFilter);
                if (aData._date > maxDateFilter) {
                    return false;
                }
            }


            return true;
        }
    );

</script>


<!-- Bootstrap -->
<script src="{{ asset('../../design/admin/vendors/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('../../design/admin/vendors/fastclick/lib/fastclick.js') }} "></script>
<!-- NProgress -->
<script src="{{ asset('../../design/admin/vendors/nprogress/nprogress.js') }} "></script>

<!-- Custom Theme Scripts -->
<script src="{{ asset('../../design/admin/build/js/custom.min.js') }} "></script>

<script src="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
@include('sweetalert::alert')


</html>
