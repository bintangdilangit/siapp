@extends('layouts.master')
@section('title')
    Detail Peminjaman
@endsection
@section('content')

    <!-- page content -->
    <div class="">
        <div class="page-title">
            <div class="title_left">
                @if ($borrow->client->role_id == 1)
                    <h3>Dari Mahasiswa</h3>
                @elseif ($borrow->client->role_id == 2)
                    <h3>Dari Tenaga Kependidikan</h3>
                @else
                    <h3>Dari Instansi Lain</h3>
                @endif

                <div class="title_left mb-3">
                    <label for="">Detail Peminjam : </label>
                    <h4>{{ $borrow->client->name }}</h4>
                    <h4>: {{ $borrow->client->noId }}</h4>

                </div>
            </div>
            <div class="title_right" style="float: right">


            </div>
            <div class="title_right mb-3">
                <div class="col-sm-12 mt-3 text-right">
                    <a href="{{ route('borrow.index') }}" class="btn btn-danger btn-xs"><i class="fa fa-mail-reply"></i>
                        Back
                    </a>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                </div>
                                <div class="card-box table-responsive">
                                    <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th scope="col">No</th>
                                                <th scope="col">Jenis Barang</th>
                                                <th scope="col">Jumlah yang dipinjam</th>
                                                <th scope="col">Status</th>
                                                <th scope="col">Aksi</th>
                                            </tr>
                                        </thead>


                                        <tbody>
                                            @foreach ($borrow->materials as $material)
                                                <tr>
                                                    <th scope="row">{{ $loop->iteration }}</th>
                                                    <th scope="row">{{ $material->name }}</th>
                                                    <th scope="row">{{ $material->pivot->borrowAmount }}</th>

                                                    <th scope="row">
                                                        @if ($material->pivot->borrowAmount != 0)
                                                            Masih dipinjam
                                                        @else
                                                            Sudah dikembalikan
                                                        @endif
                                                    </th>
                                                    <th scope="row">
                                                        <form action="{{ route('borrow.destroy', $borrow->id) }}"
                                                            method="post">
                                                            @csrf
                                                            @method('delete')
                                                            <button type="submit" class="btn btn-danger"><i
                                                                    class="fa fa-trash"></i> Hapus </button>
                                                        </form>
                                                        <a href="{{ route('edit.borrow.material', ['material' => $material, 'borrow' => $borrow]) }}"
                                                            class="btn btn-info btn-xs"><i class="fa fa-info-circle"></i>
                                                            Edit
                                                        </a>
                                                    </th>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>>
    </div>
    <!-- /page content -->

@endsection
