@extends('layouts.master')
@section('content')


    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Tambah Peminjaman</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Tambah Peminjaman</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form action="{{ route('borrow.store') }}" method="POST" novalidate>
                            @csrf
                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">No. Identitas<span
                                        class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <select class="form-control select2-single" name="noId" id="noId">
                                        @foreach ($clients as $client)
                                            <option value="{{ $client->id }}">
                                                {{ $client->noId . ' - ' . $client->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="field item form-group after-add-more">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Material<span
                                        class="required">*</span></label>
                                <div class="col-md-3 col-sm-1">
                                    <select class="select2-single form-control " name="material[]" id="id">
                                        @foreach ($materials as $material)
                                            <option value="{{ $material->id }}">
                                                {{ $material->name . '(Stock: ' . $material->stock . ')' }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label class="col-form-label col-md-1 col-sm-2  label-align">Jumlah<span
                                        class="required">*</span></label>
                                <div class="col-md-2 col-sm-1">
                                    <input class="form-control" type="number" min=1 data-validate-length-range="6"
                                        name="jumlah[]" required="required" />
                                </div>
                                <button class="btn btn-success add-more" type="button">
                                    <i class="glyphicon glyphicon-plus"></i> Add
                                </button>
                            </div>
                            <div class="copy invisible">
                                <div class="field item form-group control-group">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Material<span
                                            class="required">*</span></label>
                                    <div class="col-md-3 col-sm-1">
                                        <select class="select2-single form-control " name="material[]" id="id">
                                            @foreach ($materials as $material)
                                                <option value="{{ $material->id }}">
                                                    {{ $material->name . '(Stock: ' . $material->stock . ')' }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <label class="col-form-label col-md-1 col-sm-2  label-align">Jumlah<span
                                            class="required">*</span></label>
                                    <div class="col-md-2 col-sm-1">
                                        <input class="form-control" type="number" min=1 data-validate-length-range="6"
                                            name="jumlah[]" required="required" />
                                    </div>
                                    <button class="btn btn-danger remove" type="button"><i
                                            class="glyphicon glyphicon-remove"></i> Remove</button>
                                </div>
                            </div>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Keperluan<span
                                        class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" data-validate-length-range="6" data-validate-words="2"
                                        name="need" required="required" />
                                </div>
                            </div>
                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Digunakan
                                    di<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" data-validate-length-range="6" data-validate-words="2"
                                        name="usedIn" required="required" />
                                </div>
                            </div>
                            <div class="ln_solid">
                                <div class="form-group">
                                    <div class="col-md-6 offset-md-3 text-right">
                                        <a href="{{ route('borrow.index') }}" class="btn btn-danger btn-xs"><i
                                                class="fa fa-mail-reply"></i>
                                            Back
                                        </a>
                                        <button type='submit' class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
