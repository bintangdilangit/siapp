@extends('layouts.master')
@section('title')
    Alat Bahan
@endsection
@section('content')

    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Alat Bahan</h3>
            </div>

            <div class="title_right">
                <div class="col-sm-12 mt-3 text-right">
                    <a href="{{ route('material.create') }}" type="button" class="btn btn-success">Tambah Alat
                        Bahan</a>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>


        <div class="row">
            <div class="col-md-12 col-sm-12 ">
            </div>
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>DATA ALAT DAN BAHAN</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                    <table id="datatable-buttons" class="display table table-striped table-bordered"
                                        style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Kategori</th>
                                                <th>Nama Barang</th>
                                                <th>Stok</th>
                                                <th>Deskripsi</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($materials as $key => $material)
                                                <tr>
                                                    <th scope="row"> {{ $loop->iteration }} </th>
                                                    <td> {{ $material->kategori }} </td>
                                                    <td> {{ $material->name }} </td>
                                                    <td> {{ $material->stock }} </td>
                                                    <td> {{ $material->desc }} </td>
                                                    <td>
                                                        <a href="{{ route('material.edit', ['material' => $material]) }}"
                                                            class="btn btn-info"><i class="fa fa-pencil"></i> Edit </a>
                                                        <a href="{{ route('material.destroy', ['material' => $material]) }}"
                                                            type="button" class="btn btn-danger" data-toggle="modal"
                                                            data-target="#deleteConf{{ $material->id }}"><i
                                                                class="fa fa-trash"></i>
                                                            Delete
                                                        </a>

                                                        <!-- Modal -->
                                                        <div class="modal fade" id="deleteConf{{ $material->id }}"
                                                            tabindex="-1" role="dialog" aria-labelledby="deleteConfLabel"
                                                            aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="deleteConfLabel">
                                                                            Hapus Data Material</h5>
                                                                        <button type="button" class="close"
                                                                            data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">x</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        Yakin?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button class="btn btn-link" data-dismiss="modal">
                                                                            Cancel
                                                                        </button>
                                                                        <form
                                                                            action="{{ route('material.destroy', ['material' => $material]) }}"
                                                                            method="POST">
                                                                            @csrf
                                                                            @method('delete')
                                                                            <button type="submit"
                                                                                class="btn btn-success">Iya</button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
