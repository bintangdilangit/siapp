@extends('layouts.master')
@section('content')

    <!-- page content -->
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Tambah Alat dan Bahan</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Tambah Alat dan Bahan</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form class="" action="{{ route('material.store') }}" method="post" novalidate>
                            @csrf
                            <div class="field item form-group after-add-more">
                                <label class="col-form-label col-md-1 col-sm-1  label-align">Material<span
                                        class="required">*</span></label>
                                <div class="col-md-2 col-sm-1">
                                    <select class="select2-single form-control " name="kategori[]" id="noId">
                                        <option value="tool">Alat</option>
                                        <option value="stuff">Bahan</option>
                                    </select>
                                </div>
                                <label class="col-form-label col-md-1 col-sm-2  label-align">Nama<span
                                        class="required">*</span></label>
                                <div class="col-md-2 col-sm-1">
                                    <input class="form-control" data-validate-length-range="6" name="name[]"
                                        required="required" />
                                </div>
                                <label class="col-form-label col-md-1 col-sm-2  label-align">Jumlah<span
                                        class="required">*</span></label>
                                <div class="col-md-1 col-sm-1">
                                    <input class="form-control" type="number" min=1 data-validate-length-range="6"
                                        type="number" name="stock[]" required="required" />
                                </div>
                                <label class="col-form-label col-md-1 col-sm-2  label-align">Deskripsi<span
                                        class="required">*</span></label>
                                <div class="col-md-2 col-sm-1">
                                    <textarea class="form-control" data-validate-length-range="6" name="desc[]"
                                        required="required"> </textarea>
                                </div>
                                <div class="div">
                                    <button class="btn btn-success add-more" type="button" wire:click.prevent="addMaterial">
                                        <i class="glyphicon glyphicon-plus"></i> Add
                                    </button>
                                </div>
                            </div>

                            <div class="copy invisible">
                                <div class="field item form-group control-group">
                                    <label class="col-form-label col-md-1 col-sm-1  label-align">Material<span
                                            class="required">*</span></label>
                                    <div class="col-md-2 col-sm-1">
                                        <select class="select2-single form-control " name="kategori[]" id="noId">
                                            <option value="tool">Alat</option>
                                            <option value="stuff">Bahan</option>
                                        </select>
                                    </div>
                                    <label class="col-form-label col-md-1 col-sm-2  label-align">Nama<span
                                            class="required">*</span></label>
                                    <div class="col-md-2 col-sm-1">
                                        <input class="form-control" data-validate-length-range="6" name="name[]"
                                            required="required" />
                                    </div>
                                    <label class="col-form-label col-md-1 col-sm-2  label-align">Jumlah<span
                                            class="required">*</span></label>
                                    <div class="col-md-1 col-sm-1">
                                        <input class="form-control" type="number" min=1 data-validate-length-range="6"
                                            type="number" name="stock[]" required="required" />
                                    </div>
                                    <label class="col-form-label col-md-1 col-sm-2  label-align">Deskripsi<span
                                            class="required">*</span></label>
                                    <div class="col-md-2 col-sm-1">
                                        <textarea class="form-control" data-validate-length-range="6" name="desc[]"
                                            required="required"> </textarea>
                                    </div>
                                    <div class="div">
                                        <button class="btn btn-danger remove" type="button"><i
                                                class="glyphicon glyphicon-remove" wire:click.prevent="removeMaterial"></i>
                                            Remove</button>
                                    </div>
                                </div>
                            </div>
                            <div class="ln_solid">
                                <div class="form-group">
                                    <div class="col-md-6 offset-md-6 text-right">
                                        <button type='submit' class="btn btn-primary">Submit</button>
                                        <a href="{{ route('material.index') }}" class="btn btn-danger">Back</a>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

@endsection
