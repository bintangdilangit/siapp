@extends('layouts.master')
@section('title')
    Instansi Lain
@endsection
@section('content')

    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>INSTANSI LAIN</h3>
            </div>
            <div class="title_right mb-3">
                <div class="col-sm-12 mt-3 text-right">
                    <a href="{{ route('client.instansi.create') }}" type="button" class="btn btn-success">Tambah Data
                        Instansi Lain</a>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>


        <div class="row">
            <div class="col-md-12 col-sm-12 ">
            </div>
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>DATA INSTANSI LAIN</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                    <table id="datatable-buttons" class="table table-striped table-bordered"
                                        style="width:100%">
                                        <thead>
                                            <tr>
                                                <th scope="col">No</th>
                                                <th scope="col">NIK</th>
                                                <th scope="col">Nama</th>
                                                <th scope="col">No. Telepon</th>
                                                <th scope="col">Alamat</th>
                                                <th scope="col">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($instansi as $key => $instansi)
                                                <tr>
                                                    <th scope="row"> {{ $loop->iteration }} </th>
                                                    <td> {{ $instansi->noId }} </td>
                                                    <td> {{ $instansi->name }} </td>
                                                    <td> {{ $instansi->tlp }} </td>
                                                    <td> {{ $instansi->address }} </td>
                                                    <td>
                                                        <a href="{{ route('client.instansi.edit', ['instansi' => $instansi]) }}"
                                                            class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit
                                                        </a>
                                                        <a href="{{ route('client.instansi.destroy', ['instansi' => $instansi]) }}"
                                                            type="button" class="btn btn-danger" data-toggle="modal"
                                                            data-target="#deleteConf{{ $instansi->id }}"><i
                                                                class="fa fa-trash"></i>
                                                            Delete
                                                        </a>

                                                        <!-- Modal -->
                                                        <div class="modal fade" id="deleteConf{{ $instansi->id }}"
                                                            tabindex="-1" role="dialog" aria-labelledby="deleteConfLabel"
                                                            aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="deleteConfLabel">
                                                                            Hapus Data Instansi</h5>
                                                                        <button type="button" class="close"
                                                                            data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">x</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        Yakin?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button class="btn btn-link" data-dismiss="modal">
                                                                            Cancel
                                                                        </button>
                                                                        <form
                                                                            action="{{ route('client.instansi.destroy', ['instansi' => $instansi]) }}"
                                                                            method="POST">
                                                                            @csrf
                                                                            @method('delete')
                                                                            <button type="submit"
                                                                                class="btn btn-success">Iya</button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
