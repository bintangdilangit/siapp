@extends('layouts.master')
@section('title')
    Edit Data Tenaga Kep.
@endsection
@section('content')

    <!-- page content -->
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Edit Data Tenaga Kependidikan</h3>
                <h2>{{ $client->noId }} - {{ $client->name }}</h2>
            </div>

            <div class="title_right">
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Form</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form action="{{ route('client.dosen.update', $client->id) }}" method="POST" data-parsley-validate
                            class="form-horizontal form-label-left">
                            @method('PUT')
                            @csrf
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">NIP
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="text" name="noId" id="noId" required="required" class="form-control"
                                        value="{{ $client->noId }}">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">Nama <span
                                        class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input type="text" id="name" name="name" required="required" class="form-control"
                                        value="{{ $client->name }}">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nomor
                                    Telp.</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <input id="tlp" class="form-control" type="text" name="tlp"
                                        value="{{ $client->tlp }}">
                                </div>
                            </div>
                            <div class="item form-group">
                                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Alamat</label>
                                <div class="col-md-6 col-sm-6 ">
                                    <textarea class="form-control" name="address" id="address" cols="30"
                                        rows="5">{{ $client->address }}</textarea>
                                </div>
                            </div>
                            <div class="ln_solid"></div>
                            <div class="item form-group">
                                <div class="col-md-6 col-sm-6 offset-md-3">
                                    <a class="btn btn-danger" type="button"
                                        href="{{ route('client.dosen.index') }}">Cancel</a>
                                    <button class="btn btn-primary" type="reset">Reset</button>
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

@endsection
