<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title></title>
    <script src="http://code.jquery.com/jquery-2.0.3.min.js" data-semver="2.0.3" data-require="jquery"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/datatables/1.9.4/css/jquery.dataTables_themeroller.css"
        rel="stylesheet" data-semver="1.9.4" data-require="datatables@*" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/datatables/1.9.4/css/jquery.dataTables.css" rel="stylesheet"
        data-semver="1.9.4" data-require="datatables@*" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/datatables/1.9.4/css/demo_table_jui.css" rel="stylesheet"
        data-semver="1.9.4" data-require="datatables@*" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/datatables/1.9.4/css/demo_table.css" rel="stylesheet"
        data-semver="1.9.4" data-require="datatables@*" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/datatables/1.9.4/css/demo_page.css" rel="stylesheet"
        data-semver="1.9.4" data-require="datatables@*" />
    <link data-require="jqueryui@*" data-semver="1.10.0" rel="stylesheet"
        href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.10.0/css/smoothness/jquery-ui-1.10.0.custom.min.css" />
    <script data-require="jqueryui@*" data-semver="1.10.0"
        src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.10.0/jquery-ui.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/datatables/1.9.4/jquery.dataTables.js" data-semver="1.9.4"
        data-require="datatables@*"></script>




    <!-- buttons -->
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css"
        href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>




    <!-- Bootstrap -->
    <link href="{{ asset('../../../../design/admin/vendors/bootstrap/dist/css/bootstrap.min.css') }} "
        rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('../../../../design/admin/vendors/font-awesome/css/font-awesome.min.css') }} "
        rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('../../../../design/admin/vendors/nprogress/nprogress.css') }} " rel="stylesheet">
    <!-- jQuery custom content scroller -->

    <link href="{{ asset('../../../../design/admin/build/css/custom.min.css') }}" rel="stylesheet">


    <style>
        h1 {
            color: red;
        }

    </style>
</head>

<body>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col menu_fixed">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="index.html" class="site_title"><i class="fa fa-stethoscope"></i> <span>Sistem
                                    Informasi
                                    Laboratorium</span></a>
                        </div>

                        <div class="clearfix"></div>

                        <!-- menu profile quick info -->
                        <div class="profile clearfix">
                            <div class="profile_pic">
                                <img src="{{ asset('../../../design/admin/production/images/img.jpg') }}" alt="..."
                                    class="img-circle profile_img">
                            </div>
                            <div class="profile_info">
                                <span>Welcome,</span>
                                <h2>{{ Auth::user()->name }}</h2>
                            </div>
                        </div>
                        <!-- /menu profile quick info -->

                        <br />

                        <!-- sidebar menu -->
                        @include('layouts.module.sidebar')
                        <!-- /sidebar menu -->

                        <!-- /menu footer buttons -->

                        <!-- /menu footer buttons -->
                    </div>
                </div>

                <!-- top navigation -->
                @include('layouts.module.navbar')
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="">
                        <div class="page-title">
                            <div class="title_left">
                                <h3>Pengunjung</h3>
                            </div>
                        </div>

                        <div class="clearfix"></div>


                        <div class="row">
                            <div class="col-md-12 col-sm-12 ">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>DATA ABSENSI PENGUNJUNG</h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <h2 style="font-size: 0.7rem">Date Format : MM/DD/YYYY</h2>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <p id="date_filter">
                                                    <span id="date-label-from" class="date-label">From:
                                                    </span><input class="date_range_filter date" type="text"
                                                        id="datepicker_from" />
                                                    <span id="date-label-to" class="date-label">To:<input
                                                            class="date_range_filter date" type="text"
                                                            id="datepicker_to" />
                                                        {{-- <button type="button" name="filter" id="filter"
                                                            class="btn btn-info btn-sm">Filter</button>
                                                        <button type="button" name="refresh" id="refresh"
                                                            class="btn btn-warning btn-sm">Refresh</button> --}}
                                                </p>

                                                <table id="datatable" class="display table table-striped table-bordered"
                                                    style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">No</th>
                                                            <th scope="col">Hari Tanggal</th>
                                                            <th scope="col">Nomor Identitas</th>
                                                            <th scope="col">Nama Pengunjung</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($visitors as $key => $visitor)
                                                            <tr>
                                                                <th scope="row">{{ $loop->iteration }} </th>
                                                                <td>{{ date('m/d/Y', strtotime($visitor->created_at)) }}
                                                                </td>
                                                                <td>{{ $visitor->identityNum }}</td>
                                                                <td>{{ $visitor->name }} </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </body>

</body>

<script>
    $(function() {

        $("#datepicker_from").datepicker({
            showOn: "button",
            buttonImage: "",
            buttonImageOnly: false,
            "onSelect": function(date) {
                minDateFilter = new Date(date).getTime();
                oTable.fnDraw();
            }
        }).keyup(function() {
            minDateFilter = new Date(this.value).getTime();
            oTable.fnDraw();
        });

        $("#datepicker_to").datepicker({
            showOn: "button",
            buttonImage: "",
            buttonImageOnly: false,
            "onSelect": function(date) {
                maxDateFilter = new Date(date).getTime();
                oTable.fnDraw();
            }
        }).keyup(function() {
            maxDateFilter = new Date(this.value).getTime();
            oTable.fnDraw();
        });
        var oTable = $('#datatable').DataTable({
            dom: 'Bfrtip',
            "buttons": [
                {
                    extend: 'copyHtml5',
                    title: 'Rekap Absensi Pengunjung'
                },
                {
                    extend: 'excelHtml5',
                    title: 'Rekap Absensi Pengunjung'
                },
                {
                    extend: 'pdfHtml5',
                    title: 'Rekap Absensi Pengunjung'
                },
                {
                    extend: 'csvHtml5',
                    title: 'Rekap Absensi Pengunjung'
                },
                {
                    extend: 'print',
                    title: 'Rekap Absensi Pengunjung'
                },
            ],
            "oLanguage": {
                "sSearch": "Search<br>"
            },
            "iDisplayLength": 10,
            "sPaginationType": "full_numbers",

        });

    });


    // Date range filter
    minDateFilter = "";
    maxDateFilter = "";

    $.fn.dataTableExt.afnFiltering.push(
        function(oSettings, aData, iDataIndex) {
            if (typeof aData._date == 'undefined') {
                aData._date = new Date(aData[1]).getTime();
            }

            if (minDateFilter && !isNaN(minDateFilter)) {
                console.log(minDateFilter && !isNaN(minDateFilter));
                if (aData._date < minDateFilter) {
                    return false;
                }
            }

            if (maxDateFilter && !isNaN(maxDateFilter)) {
                console.log(maxDateFilter);
                if (aData._date > maxDateFilter) {
                    return false;
                }
            }


            return true;
        }
    );

    $('#refresh').click(function() {
        $('#datepicker_from').val('');
        $('#datepicker_to').val('');
        fetch_data();
    });

</script>


<!-- Bootstrap -->
<script src="{{ asset('../../design/admin/vendors/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('../../design/admin/vendors/fastclick/lib/fastclick.js') }} "></script>
<!-- NProgress -->
<script src="{{ asset('../../design/admin/vendors/nprogress/nprogress.js') }} "></script>

<!-- Custom Theme Scripts -->
<script src="{{ asset('../../design/admin/build/js/custom.min.js') }} "></script>

<script src="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>



</html>
