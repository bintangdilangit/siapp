@extends('layouts.master')
@section('title')
    Dashboard
@endsection
@section('content')
    <div class="row" style="display: inline-block;">
        <div class="tile_count">
            <div class="col-md-3 col-sm-4  tile_stats_count">
                <span class="count_top"><i class="fa fa-line-chart"></i> Total Peminjaman</span>
                <div class="count green">{{ $countPjl }}</div>
            </div>
            <div class="col-md-3 col-sm-4  tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Total Dosen</span>
                <div class="count">{{ $countDosen }}</div>
            </div>
            <div class="col-md-3 col-sm-4  tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Total Mahasiswa</span>
                <div class="count">{{ $countMhs }}</div>
            </div>
            <div class="col-md-3 col-sm-4  tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Total Instansi Lain</span>
                <div class="count">{{ $countIns }}</div>
            </div>
        </div>
    </div>
    <br />

    <div class="row">
        <div class="col-md-6 col-sm-6  ">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Client</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <div id="pie_basic" style="height:350px;"></div>

                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6  ">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Transaksi</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <div id="pie_basic2" style="height:350px;"></div>

                </div>
            </div>
        </div>

    </div>

    <!-- /top tiles -->


@endsection
@push('scp')
    <script type="text/javascript">
        var pie_basic_element = document.getElementById('pie_basic');
        if (pie_basic_element) {
            var pie_basic = echarts.init(pie_basic_element);
            pie_basic.setOption({
                color: [
                    '#07a2a4', '#d87a80', '#dc69aa', '#ffb980', '#d87a80',
                    '#8d98b3', '#e5cf0d', '#97b552', '#95706d', '#dc69aa',
                    '#07a2a4', '#9a7fd1', '#588dd5', '#f5994e', '#c05050',
                    '#59678c', '#c9ab00', '#7eb00a', '#6f5553', '#c14089'
                ],

                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                title: {
                    text: 'Jumlah Client',
                    left: 'center',
                    textStyle: {
                        fontSize: 17,
                        fontWeight: 500
                    },
                    subtextStyle: {
                        fontSize: 12
                    }
                },

                tooltip: {
                    trigger: 'item',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },

                legend: {
                    orient: 'horizontal',
                    bottom: '0%',
                    left: 'center',
                    data: ['Mahasiswa', 'Tenaga Kependidikan', 'Instansi Lain'],
                    itemHeight: 8,
                    itemWidth: 8
                },

                series: [{
                    name: 'Client',
                    type: 'pie',
                    radius: '70%',
                    center: ['50%', '50%'],
                    itemStyle: {
                        normal: {
                            borderWidth: 1,
                            borderColor: '#fff'
                        }
                    },
                    data: [{
                            value: {{ $countMhs }},
                            name: 'Mahasiswa'
                        },
                        {
                            value: {{ $countDosen }},
                            name: 'Tenaga Kependidikan'
                        },
                        {
                            value: {{ $countIns }},
                            name: 'Instansi Lain'
                        }
                    ]
                }]
            });
        }

    </script>
    <script type="text/javascript">
        var pie_basic_element = document.getElementById('pie_basic2');
        if (pie_basic_element) {
            var pie_basic2 = echarts.init(pie_basic_element);
            pie_basic2.setOption({
                color: [
                    '#48bf91', '#006994', '#5ab1ef', '#ffb980', '#d87a80',
                    '#8d98b3', '#e5cf0d', '#97b552', '#95706d', '#dc69aa',
                    '#07a2a4', '#9a7fd1', '#588dd5', '#f5994e', '#c05050',
                    '#59678c', '#c9ab00', '#7eb00a', '#6f5553', '#c14089'
                ],

                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                title: {
                    text: 'Rekap Transaksi',
                    left: 'center',
                    textStyle: {
                        fontSize: 17,
                        fontWeight: 500
                    },
                    subtextStyle: {
                        fontSize: 12
                    }
                },

                tooltip: {
                    trigger: 'item',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },

                legend: {
                    orient: 'horizontal',
                    bottom: '0%',
                    left: 'center',
                    data: ['Peminjaman', 'Pengembalian'],
                    itemHeight: 8,
                    itemWidth: 8
                },

                series: [{
                    name: 'Transaksi',
                    type: 'pie',
                    radius: '70%',
                    center: ['50%', '50%'],
                    itemStyle: {
                        normal: {
                            borderWidth: 1,
                            borderColor: '#fff'
                        }
                    },
                    data: [{
                            value: {{ $countPjl }},
                            name: 'Peminjaman'
                        },
                        {
                            value: {{ $countPng }},
                            name: 'Pengembalian'
                        },
                    ]
                }]
            });
        }

    </script>


@endpush
