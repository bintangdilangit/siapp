<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-home"></i> Home <span></span></a>
            </li>
            <li><a href="{{ route('material.index') }}"><i class="fa fa-cubes"></i> Material <span></span></a>
            </li>
            <li><a><i class="fa fa-edit"></i> Absensi <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ route('practice.index') }}">Absensi Praktikum</a></li>
                    <li><a href="{{ route('visitor.index') }}">Absensi Pengunjung</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-shopping-cart"></i> Transaksi <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ route('borrow.index') }}">Peminjaman</a></li>
                    <li><a href="{{ route('reverse.index') }}">Pengembalian</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-users"></i> Data Client <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{ route('client.dosen.index') }}">Tenaga Kependidikan</a></li>
                    <li><a href="{{ route('client.mahasiswa.index') }}">Mahasiswa</a></li>
                    <li><a href="{{ route('client.instansi.index') }}">Instansi Lain</a></li>
                </ul>
            </li>
        </ul>
    </div>

</div>
