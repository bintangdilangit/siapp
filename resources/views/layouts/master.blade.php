<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    {{-- Select 2 --}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    {{-- End Select 2 --}}

    <!-- Bootstrap -->
    <link href="{{ asset('../../design/admin/cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css') }}">
    <link href="{{ asset('../../design/admin/vendors/bootstrap/dist/css/bootstrap.min.css') }} " rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('../../design/admin/vendors/font-awesome/css/font-awesome.min.css') }} " rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('../../design/admin/vendors/nprogress/nprogress.css') }} " rel="stylesheet">
    <!-- iCheck -->
    <link href="{{ asset('../../design/admin/vendors/iCheck/skins/flat/green.css') }}" rel="stylesheet">
    <!-- Datatables -->

    <link href="{{ asset('../../design/admin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }} "
        rel="stylesheet">
    <link href="{{ asset('../../design/admin/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }} "
        rel="stylesheet">
    <link
        href="{{ asset('../../design/admin/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }} "
        rel="stylesheet">
    <link
        href="{{ asset('../../design/admin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }} "
        rel="stylesheet">
    <link href="{{ asset('../../design/admin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }} "
        rel="stylesheet">
    <!-- Calendar -->
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset('../../design/admin/vendors/bootstrap-daterangepicker/daterangepicker.css') }}"
        rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link
        href="{{ asset('../../design/admin/vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css') }} "
        rel="stylesheet">

    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset('../../design/admin/vendors/bootstrap-daterangepicker/daterangepicker.css') }}"
        rel="stylesheet">

    <!-- bootstrap-progressbar -->
    <link
        href="{{ asset('../../design/admin/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') }}"
        rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{ asset('../../design/admin/vendors/jqvmap/dist/jqvmap.min.css') }}" rel="stylesheet" />

    {{-- LiveWire --}}
    @livewireStyles
    {{-- End LiveWire --}}

    <!-- Custom Theme Style -->
    <link href="{{ asset('../../design/admin/build/css/custom.min.css') }}" rel="stylesheet">
    <script src="{{ asset('../../design/admin/vendors/jquery/dist/jquery.min.js') }}"></script>


</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col menu_fixed">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index.html" class="site_title"><i class="fa fa-stethoscope"></i> <span>Sistem Informasi
                                Laboratorium</span></a>
                    </div>

                    <div class="clearfix"></div>

                    <!-- menu profile quick info -->
                    <div class="profile clearfix">
                        <div class="profile_pic">
                            <img src="{{ asset('../../../design/admin/production/images/img.jpg') }}" alt="..."
                                class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2>{{ Auth::user()->name }}</h2>
                        </div>
                    </div>
                    <!-- /menu profile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    @include('layouts.module.sidebar')
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->

                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            @include('layouts.module.navbar')
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                @yield('content')
            </div>
            <!-- /page content -->

            <!-- footer content -->
            {{-- @include('layouts.module.footer') --}}
            <!-- /footer content -->
        </div>
    </div>

    {{-- LiveWire --}}
    @livewireScripts
    {{-- End LiveWire --}}
    <!-- jQuery -->
    <script src="{{ asset('../../design/admin/vendors/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('../../design/admin/vendors/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('../../design/admin/vendors/fastclick/lib/fastclick.js') }} "></script>
    <!-- NProgress -->
    <script src="{{ asset('../../design/admin/vendors/nprogress/nprogress.js') }} "></script>
    <!-- iCheck -->
    <script src="{{ asset('../../design/admin/vendors/iCheck/icheck.min.js') }}"></script>
    <!-- Datatables -->
    <script src="{{ asset('../../design/admin/vendors/datatables.net/js/jquery.dataTables.min.js') }} "></script>
    <script src="{{ asset('../../design/admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }} ">
    </script>
    <script src="{{ asset('../../design/admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }} ">
    </script>
    <script src="{{ asset('../../design/admin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }} ">
    </script>
    <script src="{{ asset('../../design/admin/vendors/datatables.net-buttons/js/buttons.flash.min.js') }} "></script>
    <script src="{{ asset('../../design/admin/vendors/datatables.net-buttons/js/buttons.html5.min.js') }} "></script>
    <script src="{{ asset('../../design/admin/vendors/datatables.net-buttons/js/buttons.print.min.js') }} "></script>
    <script
        src="{{ asset('../../design/admin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }} ">
    </script>
    <script src="{{ asset('../../design/admin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }} ">
    </script>
    <script
        src="{{ asset('../../design/admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }} ">
    </script>
    <script src="{{ asset('../../design/admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }} ">
    </script>
    <script src="{{ asset('../../design/admin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }} ">
    </script>
    <script src="{{ asset('../../design/admin/vendors/jszip/dist/jszip.min.js') }} "></script>
    <script src="{{ asset('../../design/admin/vendors/pdfmake/build/pdfmake.min.js') }} "></script>
    <script src="{{ asset('../../design/admin/vendors/pdfmake/build/vfs_fonts.js') }} "></script>
    <!-- jQuery -->
    {{-- <script src="{{ asset('../../design/admin/vendors/jquery/dist/jquery.min.js') }}"></script> --}}

    <!-- Custom Theme Scripts -->
    <script src="{{ asset('../../design/admin/build/js/custom.min.js') }} "></script>



    <script src="{{ asset('../../design/admin/vendors/Chart.js/dist/Chart.min.js') }} "></script>
    <script src="{{ asset('../../design/admin/vendors/gauge.js/dist/gauge.min.js') }} "></script>
    <script src="{{ asset('../../design/admin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }} ">
    </script>

    <script src="{{ asset('../../design/admin/vendors/skycons/skycons.js') }} "></script>
    <script src="{{ asset('../../design/admin/vendors/Flot/jquery.flot.js') }} "></script>
    <script src="{{ asset('../../design/admin/vendors/Flot/jquery.flot.pie.js') }} "></script>
    <script src="{{ asset('../../design/admin/vendors/Flot/jquery.flot.time.js') }} "></script>
    <script src="{{ asset('../../design/admin/vendors/Flot/jquery.flot.stack.js') }} "></script>
    <script src="{{ asset('../../design/admin/vendors/Flot/jquery.flot.resize.js') }} "></script>

    <script src="{{ asset('../../design/admin/vendors/flot.orderbars/js/jquery.flot.orderBars.js') }} "></script>
    <script src="{{ asset('../../design/admin/vendors/flot-spline/js/jquery.flot.spline.min.js') }} "></script>
    <script src="{{ asset('../../design/admin/vendors/flot.curvedlines/curvedLines.js') }} "></script>

    <!-- DateJS -->
    <script src="{{ asset('../../design/admin/vendors/DateJS/build/date.js') }} "></script>
    <!-- JQVMap -->
    <script src="{{ asset('../../design/admin/vendors/jqvmap/dist/jquery.vmap.js') }} "></script>
    <script src="{{ asset('../../design/admin/vendors/jqvmap/dist/maps/jquery.vmap.world.js') }} "></script>
    <script src="{{ asset('../../design/admin/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js') }} "></script>
    <!-- Calendar -->
    <script src="{{ asset('../../design/admin/vendors/moment/min/moment.min.js') }} "></script>
    <script src="{{ asset('../../design/admin/vendors/bootstrap-daterangepicker/daterangepicker.js') }} "></script>
    <!-- bootstrap-datetimepicker -->
    <script
        src="{{ asset('../../design/admin/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }} ">
    </script>



    <!-- jQuery -->
    {{-- <script src="{{ asset('../../design/admin/vendors/jquery/dist/jquery.min.js') }}"></script> --}}

    <script src="{{ asset('../../design/admin/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="{{ asset('../../design/admin/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js') }}"></script>
    <script src="{{ asset('../../design/admin/vendors/jquery.hotkeys/jquery.hotkeys.js') }}"></script>
    <script src="{{ asset('../../design/admin/vendors/google-code-prettify/src/prettify.js') }}"></script>
    <!-- jQuery Tags Input -->
    <script src="{{ asset('../../design/admin/vendors/jquery.tagsinput/src/jquery.tagsinput.js') }}"></script>
    <!-- Switchery -->
    <script src="{{ asset('../../design/admin/vendors/switchery/dist/switchery.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('../../design/admin/vendors/select2/dist/js/select2.full.min.js') }}"></script>
    <!-- Parsley -->
    <script src="{{ asset('../../design/admin/vendors/parsleyjs/dist/parsley.min.js') }}"></script>
    <!-- Autosize -->
    <script src="{{ asset('../../design/admin/vendors/autosize/dist/autosize.min.js') }}"></script>
    <!-- jQuery autocomplete -->
    <script src="{{ asset('../../design/admin/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js') }}">
    </script>
    <!-- starrr -->
    <script src="{{ asset('../../design/admin/vendors/starrr/dist/starrr.js') }}"></script>

    {{-- eChart --}}
    <!-- FastClick -->
    {{-- <script src="{{ asset('../../design/admin/vendors/fastclick/lib/fastclick.js') }}"></script> --}}
    <!-- NProgress -->
    {{-- <script src="{{ asset('../../design/admin/vendors/nprogress/nprogress.js') }}"></script> --}}
    <!-- ECharts -->
    <script src="{{ asset('../../design/admin/vendors/echarts/dist/echarts.min.js') }}"></script>
    {{-- End eChart --}}

    <script type="text/javascript">
        $(document).ready(function() {
            $(".add-more").click(function() {
                var html = $(".copy").html();
                $(".after-add-more").after(html);
            });

            // saat tombol remove dklik control group akan dihapus
            $("body").on("click", ".remove", function() {
                $(this).parents(".control-group").remove();
            });
        });

    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".add-more1").click(function() {
                var html = $(".copy").html();
                $(".after-add-more1").after(html);
            });

            // saat tombol remove dklik control group akan dihapus
            $("body").on("click", ".remove", function() {
                $(this).parents(".control-group1").remove();
            });
        });

    </script>woala


    <!-- Initialize datetimepicker -->
    <script type="text/javascript">
        $(function() {
            $('#myDatepicker').datetimepicker();
        });

        $('#myDatepicker2').datetimepicker({
            format: 'DD.MM.YYYY'
        });

        $('#myDatepicker3').datetimepicker({
            format: 'hh:mm A'
        });

        $('#myDatepicker4').datetimepicker({
            ignoreReadonly: true,
            allowInputToggle: true
        });

        $('#datetimepicker6').datetimepicker();

        $('#datetimepicker7').datetimepicker({
            useCurrent: false
        });

        $("#datetimepicker6").on("dp.change", function(e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });

        $("#datetimepicker7").on("dp.change", function(e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        });

    </script>

    <script>
        var myChart = echarts.init(document.getElementById('echart_bar_horizontal'), theme);
        myChart.setOption({
            title: {
                text: 'Bar Graph',
                subtext: 'Graph subtitle'
            },
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data: ['2011', '2012']
            },
            toolbox: {
                show: true,
                feature: {
                    saveAsImage: {
                        show: true
                    }
                }
            },
            calculable: true,
            xAxis: [{
                type: 'value',
                boundaryGap: [0, 0.01]
            }],
            yAxis: [{
                type: 'category',
                data: ['Woi', 'Feb', 'Mar', 'Apr', 'May', 'Jun']
            }],
            series: [{
                    name: '2011',
                    type: 'bar',
                    data: [18203, 23489, 29034, 104970, 131744, 630230]
                },
                {
                    name: '2012',
                    type: 'bar',
                    data: [19325, 23438, 31000, 121594, 134141, 681807]
                }
            ]
        });

    </script>
    @include('sweetalert::alert')

    @stack('scp')


</body>

</html>
