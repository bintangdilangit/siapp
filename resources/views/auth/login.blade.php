<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentelella Alela! | </title>

    <!-- Bootstrap -->
    <link href="{{ asset('../../design/admin/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('../../design/admin/vendors/font-awesome/css/font-awesome.min.css') }} " rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('../../design/admin/vendors/nprogress/nprogress.css') }} " rel="stylesheet">
    <!-- Animate.css -->
    <link href="{{ asset('../../design/admin/vendors/animate.css/animate.min.css') }} " rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ asset('../../design/admin/build/css/custom.min.css') }} " rel="stylesheet">
</head>

<body class="login">
    <div>
        <a class="hiddenanchor" id="signup"></a>
        <a class="hiddenanchor" id="signin"></a>

        <div class="login_wrapper">
            <div class="animate form login_form">
                <section class="login_content">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <h1>Login Form</h1>
                        <div>
                            <input id="email" class="form-control" placeholder="Email" type="email" name="email"
                                :value="old('email')" required autofocus />
                        </div>
                        <div>
                            <input id="password" type="password" class="form-control" placeholder="Password"
                                name="password" required autocomplete="current-password" />
                        </div>
                        <!-- Remember Me -->
                        <div>
                            <label for="remember_me" class="inline-flex items-center">
                                <input id="remember_me" type="checkbox"
                                    class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                                    name="remember">
                                <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                            </label>
                        </div>
                        <div>
                            <button class="btn btn-default submit" type="submit">Login</button>
                        </div>

                        <div class="clearfix"></div>

                        <div class="separator">
                            <a href="#signup" class="to_register"> Create Account </a>
                            </p>

                            <div class="clearfix"></div>
                            <br />

                            <div>
                                <h1><i class="fa fa-stethoscope"></i>Sistem Informasi Laboratorium</h1>

                            </div>
                        </div>
                    </form>
                </section>
            </div>

            <div id="register" class="animate form registration_form">
                <section class="login_content">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <h1>Create Account</h1>
                        <div>
                            <input type="text" :value="old('name')" class="form-control" placeholder="Username"
                                id="name" name="name" required autofocus />
                        </div>
                        <div>
                            <input type="email" :value="old('email')" id="email" class="form-control"
                                placeholder="Email" name="email" required />
                        </div>
                        <div>
                            <input id="password" type="password" class="form-control" placeholder="Password"
                                name="password" required autocomplete="new-password" />
                        </div>
                        <div>
                            <input id="password_confirmation" type="password" class="form-control"
                                placeholder="Confirm Password" name="password_confirmation" required />
                        </div>
                        <div>
                            <button class="btn btn-default submit" type="submit">Register</button>
                        </div>

                        <div class="clearfix"></div>

                        <div class="separator">
                            <p class="change_link">Already a member ?
                                <a href="#signin" class="to_register"> Log in </a>
                            </p>

                            <div class="clearfix"></div>
                            <br />

                            <div>
                                <h1><i class="fa fa-stethoscope"></i> Sistem Informasi Laboratorium</h1>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</body>

</html>
