<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Sistem Informasi Laboratorium</title>

    <!-- Bootstrap -->
    <link href="{{ asset('../../design/admin/vendors/bootstrap/dist/css/bootstrap.min.css') }} " rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('../../design/admin/vendors/font-awesome/css/font-awesome.min.css') }} " rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('../../design/admin/vendors/nprogress/nprogress.css') }} " rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset('../../design/admin/build/css/custom.min.css') }}" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body class="login ">
    <div class="wrapper">
        <div class="animate form login_form ">
            <section class="login_content">
                <form method="POST" action="{{ route('practice.store') }}">
                    @csrf
                    <h1>Practice Attendance</h1>
                    <div class="col-md-2 col-sm-12">
                        <div>
                            <input id="course" class="form-control" placeholder="Mata Kuliah" type="text" name="course"
                                required autofocus autocomplete="off" />
                        </div>
                        <div>
                            <input id="groupName" type="number" min="0" class="form-control" placeholder="Kelompok"
                                name="groupName" required autofocus autocomplete="off" />
                        </div>
                        <br>
                        <div>
                            <input id="action" type="text" class="form-control" placeholder="Tindakan" name="action"
                                required autofocus autocomplete="off" />
                        </div>
                        <div>
                            <input id="lecturerName" type="text" class="form-control" placeholder="Nama Dosen"
                                name="lecturerName" required autofocus autocomplete="off" />
                            <div id="lecturer_list"></div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-12">
                        <div>
                            <input id="member1" type="text" class="form-control" placeholder="Nama Anggota 1"
                                name="member1" required autofocus autocomplete="off">
                            <div id="member_list1"></div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div>
                            <input id="member2" type="text" class="form-control" placeholder="Nama Anggota 2"
                                name="member2" autofocus autocomplete="off">
                            <div id="member_list2"></div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div>
                            <input id="member3" type="text" class="form-control" placeholder="Nama Anggota 3"
                                name="member3" autofocus autocomplete="off">
                            <div id="member_list3"></div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div>
                            <input id="member4" type="text" class="form-control" placeholder="Nama Anggota 4"
                                name="member4" autofocus autocomplete="off">
                            <div id="member_list4"></div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div>
                            <input id="member5" type="text" class="form-control" placeholder="Nama Anggota 5"
                                name="member5" autofocus autocomplete="off">
                            <div id="member_list5"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="separator">

                        <div class="clearfix"></div>
                        <br />
                        <div>
                            <button style="font-size: 15px" class="btn btn-success" type="submit">SUBMIT</button>
                            <button style="font-size: 15px" class="btn btn-danger" onclick="location.href='/'" type="
                                submit" name="submit">BACK</button>

                        </div>

                        <div>
                            <h1><i class="fa fa-stethoscope"></i>Sistem Informasi Laboratorium</h1>

                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
    </div>
    </div>

    <!-- jQuery -->
    <script src="{{ asset('../design/admin/vendors/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('../design/admin/vendors/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('../design/admin/vendors/fastclick/lib/fastclick.js') }}"></script>
    <!-- NProgress -->
    <script src="{{ asset('../design/admin/vendors/nprogress/nprogress.js') }}"></script>
    <!-- validator -->

    <!-- Custom Theme Scripts -->
    <script src="{{ asset('../design/admin/build/js/custom.min.js') }}"></script>


    <script>
        $(document).ready(function() {

            $('#lecturerName').keyup(function() {
                var query = $(this).val();
                if (query != '') {
                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url: "{{ route('practice.create.fetch') }}",
                        method: "POST",
                        data: {
                            query: query,
                            _token: _token
                        },
                        success: function(data) {
                            $('#lecturer_list').fadeIn();
                            $('#lecturer_list').html(data);
                        }
                    });
                } else {
                    $('#lecturerName').val($(this).text());
                    $('#lecturer_list').fadeOut();
                }
            });

            $('#lecturer_list').on('click', 'li', function() {
                $('#lecturerName').val($(this).text());
                $('#lecturer_list').fadeOut();
            });

        });

    </script>
    <script>
        $(document).ready(function() {

            $('#member1').keyup(function() {
                var query = $(this).val();
                if (query != '') {
                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url: "{{ route('practice.create.fetch') }}",
                        method: "POST",
                        data: {
                            query: query,
                            _token: _token
                        },
                        success: function(data) {
                            $('#member_list1').fadeIn();
                            $('#member_list1').html(data);
                        }
                    });
                } else {
                    $('#member1').val($(this).text());
                    $('#member_list1').fadeOut();
                }
            });

            $('#member_list1').on('click', 'li', function() {
                $('#member1').val($(this).text());
                $('#member_list1').fadeOut();
            });

        });

    </script>
    <script>
        $(document).ready(function() {

            $('#member2').keyup(function() {
                var query = $(this).val();
                if (query != '') {
                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url: "{{ route('practice.create.fetch') }}",
                        method: "POST",
                        data: {
                            query: query,
                            _token: _token
                        },
                        success: function(data) {
                            $('#member_list2').fadeIn();
                            $('#member_list2').html(data);
                        }
                    });
                } else {
                    $('#member2').val($(this).text());
                    $('#member_list2').fadeOut();
                }
            });

            $('#member_list2').on('click', 'li', function() {
                $('#member2').val($(this).text());
                $('#member_list2').fadeOut();
            });

        });

    </script>
    <script>
        $(document).ready(function() {

            $('#member3').keyup(function() {
                var query = $(this).val();
                if (query != '') {
                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url: "{{ route('practice.create.fetch') }}",
                        method: "POST",
                        data: {
                            query: query,
                            _token: _token
                        },
                        success: function(data) {
                            $('#member_list3').fadeIn();
                            $('#member_list3').html(data);
                        }
                    });
                } else {
                    $('#member3').val($(this).text());
                    $('#member_list3').fadeOut();
                }
            });

            $('#member_list3').on('click', 'li', function() {
                $('#member3').val($(this).text());
                $('#member_list3').fadeOut();
            });

        });

    </script>
    <script>
        $(document).ready(function() {

            $('#member4').keyup(function() {
                var query = $(this).val();
                if (query != '') {
                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url: "{{ route('practice.create.fetch') }}",
                        method: "POST",
                        data: {
                            query: query,
                            _token: _token
                        },
                        success: function(data) {
                            $('#member_list4').fadeIn();
                            $('#member_list4').html(data);
                        }
                    });
                } else {
                    $('#member4').val($(this).text());
                    $('#member_list4').fadeOut();
                }
            });

            $('#member_list4').on('click', 'li', function() {
                $('#member4').val($(this).text());
                $('#member_list4').fadeOut();
            });

        });

    </script>
    <script>
        $(document).ready(function() {

            $('#member5').keyup(function() {
                var query = $(this).val();
                if (query != '') {
                    var _token = $('input[name="_token"]').val();
                    $.ajax({
                        url: "{{ route('practice.create.fetch') }}",
                        method: "POST",
                        data: {
                            query: query,
                            _token: _token
                        },
                        success: function(data) {
                            $('#member_list5').fadeIn();
                            $('#member_list5').html(data);
                        }
                    });
                } else {
                    $('#member5').val($(this).text());
                    $('#member_list5').fadeOut();
                }
            });

            $('#member_list5').on('click', 'li', function() {
                $('#member5').val($(this).text());
                $('#member_list5').fadeOut();
            });

        });

    </script>
    @include('sweetalert::alert')

</body>



</html>
