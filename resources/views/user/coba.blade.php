<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Sistem Informasi Laboratorium</title>

    <!-- Bootstrap -->
    <link href="{{ asset('../../design/admin/vendors/bootstrap/dist/css/bootstrap.min.css') }} " rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('../../design/admin/vendors/font-awesome/css/font-awesome.min.css') }} " rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('../../design/admin/vendors/nprogress/nprogress.css') }} " rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset('../../design/admin/build/css/custom.min.css') }}" rel="stylesheet">

    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script> --}}

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body class="login">
    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
                <form method="POST" action="{{ route('visitor.store') }}">
                    @csrf
                    <h1>Visitor Attendance</h1>
                    <div>
                        <input id="identityNum" class="form-control" placeholder="No Identitas" type="text"
                            name="identityNum" required autofocus />
                    </div>
                    <div>
                        <input id="name" type="text" class="typeahead form-control" placeholder="Nama" name="name" required
                            autofocus />
                        <div id="name_list"></div>
                    </div>
                    <br><br>
                    <div>
                        <button style="font-size: 15px" class="btn btn-success" type="submit">SUBMIT</button>
                        <button style="font-size: 15px" class="btn btn-danger" type="submit">CANCEL</button>
                    </div>

                    <div class="clearfix"></div>

                    <div class="separator">

                        <div class="clearfix"></div>
                        <br />

                        <div>
                            <h1><i class="fa fa-stethoscope"></i>Sistem Informasi Laboratorium</h1>

                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
    </div>
    </div>

    <!-- jQuery -->
    <script src="{{ asset('../design/admin/vendors/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('../design/admin/vendors/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('../design/admin/vendors/fastclick/lib/fastclick.js') }}"></script>
    <!-- NProgress -->
    <script src="{{ asset('../design/admin/vendors/nprogress/nprogress.js') }}"></script>
    <!-- validator -->
    {{-- <!-- <script src="{{ asset('../design/admin') }}vendors/validator/validator.js"></script> --> --}}

    <!-- Custom Theme Scripts -->
    <script src="{{ asset('../design/admin/build/js/custom.min.js') }}"></script>

<script>
    $(document).ready(function(){

     $('#name').keyup(function(){
            var query = $(this).val();
            if(query != '')
            {
             var _token = $('input[name="_token"]').val();
             $.ajax({
              url:"{{ route('visitor.create.fetch') }}",
              method:"POST",
              data:{query:query, _token:_token},
              success:function(data){
               $('#name_list').fadeIn();
                        $('#name_list').html(data);
              }
             });
            }
        });

        $(document).on('click', 'li', function(){
            $('#name').val($(this).text());
            $('#name_list').fadeOut();
        });

    });
    </script>

</body>

</html>
