<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function indexMahasiswa()
    {
        $mhs = Client::where('role_id','1')->orderBy('created_at', 'desc')->get();
        return view('admin.client.mahasiswa.index',compact('mhs'));
    }
    public function indexDosen()
    {
        $tkp = Client::where('role_id','2')->orderBy('created_at', 'desc')->get();
        return view('admin.client.tenagaKep.index',compact('tkp'));
    }
    public function indexInstansi()
    {
        $instansi = Client::where('role_id','3')->orderBy('created_at', 'desc')->get();
        return view('admin.client.instansiLain.index',compact('instansi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createMahasiswa()
    {
        return view('admin.client.mahasiswa.create');
    }
    public function createDosen()
    {
        return view('admin.client.tenagaKep.create');
    }
    public function createInstansi()
    {
        return view('admin.client.instansiLain.create');
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeMahasiswa(Request $request)
    {
        $this->validate(request(), [
            'noId' => 'required',
            'name' => 'required',
            'tlp' => 'required',
            'address' => 'required',
        ]);
        $client = Client::create([
            'noId' => $request->noId,
            'name' => $request->name,
            'tlp' => $request->tlp,
            'address' => $request->address,
            'role_id'=> 1,
        ]);


        toast('Your Data has been submited!','success');
        $client->save();
        return redirect(route('client.mahasiswa.index'));
    }
    public function storeDosen(Request $request)
    {
        $this->validate(request(), [
            'noId' => 'required',
            'name' => 'required',
            'tlp' => 'required',
            'address' => 'required',
        ]);
        $client = Client::create([
            'noId' => $request->noId,
            'name' => $request->name,
            'tlp' => $request->tlp,
            'address' => $request->address,
            'role_id'=> 2,
        ]);


        toast('Your Data has been submited!','success');
        $client->save();
        return redirect(route('client.dosen.index'));
    }
    public function storeInstansi(Request $request)
    {
        $this->validate(request(), [
            'noId' => 'required',
            'name' => 'required',
            'tlp' => 'required',
            'address' => 'required',
        ]);
        $client = Client::create([
            'noId' => $request->noId,
            'name' => $request->name,
            'tlp' => $request->tlp,
            'address' => $request->address,
            'role_id'=> 3,
        ]);
        toast('Your Data has been submited!','success');
        $client->save();
        return redirect(route('client.instansi.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editMahasiswa($id)
    {
        $list = Client::all();
        $client = $list->find($id);
        // dd($client);
        return view('admin.client.mahasiswa.edit',compact('client'));
    }
    public function editDosen($id)
    {
        $list = Client::all();
        $client = $list->find($id);
        return view('admin.client.tenagaKep.edit',compact('client'));
    }
    public function editInstansi($id)
    {
        $list = Client::all();
        $client = $list->find($id);
        return view('admin.client.instansiLain.edit',compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateMahasiswa(Request $request, $id)
    {
        $this->validate(request(), [
            'noId' => 'required',
            'name' => 'required',
            'tlp' => 'required',
            'address' => 'required',
        ]);
        $client = Client::find($id);

        $client->noId = $request['noId'];
        $client->name = $request['name'];
        $client->tlp = $request['tlp'];
        $client->address = $request['address'];

        $client->save();
        Alert::success('Success', 'Data Mahasiswa has been updated');
        return redirect(route('client.mahasiswa.index'));
    }
    public function updateDosen(Request $request, $id)
    {
        $this->validate(request(), [
            'noId' => 'required',
            'name' => 'required',
            'tlp' => 'required',
            'address' => 'required',
        ]);
        $client = Client::find($id);

        $client->noId = $request['noId'];
        $client->name = $request['name'];
        $client->tlp = $request['tlp'];
        $client->address = $request['address'];

        $client->save();
        Alert::success('Success', 'Data Tenaga Kependidikan has been updated');
        return redirect(route('client.dosen.index'));
    }
    public function updateInstansi(Request $request, $id)
    {
        $this->validate(request(), [
            'noId' => 'required',
            'name' => 'required',
            'tlp' => 'required',
            'address' => 'required',
        ]);
        $client = Client::find($id);

        $client->noId = $request['noId'];
        $client->name = $request['name'];
        $client->tlp = $request['tlp'];
        $client->address = $request['address'];

        $client->save();
        Alert::success('Success', 'Data Instansi Lain has been updated');
        return redirect(route('client.instansi.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyMahasiswa($id)
    {
        $client = Client::find($id);
        $client->delete();


        toast('Data mahasiswa has been deleted!','success');
        return redirect(route('client.mahasiswa.index'));
    }
    public function destroyDosen($id)
    {
        $client = Client::find($id);
        $client->delete();
        toast('Data tenaga kependidikan has been deleted!','success');
        return redirect(route('client.dosen.index'));
    }
    public function destroyInstansi($id)
    {
        $client = Client::find($id);
        // $client->tags()->detach();
        // $answer = Answer::where('question_id', $id)->delete();
        // $comment = Comment::find($id);
        // $comment->comment()->detach();
        $client->delete();
        toast('Data instansi lain has been deleted!','success');
        return redirect(route('client.instansi.index'));
    }
}
