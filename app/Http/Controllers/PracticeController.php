<?php

namespace App\Http\Controllers;

use App\Models\Practice;
use App\Models\Visitors;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class PracticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth')->except('create','store');
    }
    public function index()
    {
        $practice = Practice::orderBy('created_at', 'desc')->get();
        return view('admin.attendance.practice',compact('practice'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.practice');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'course' => 'required',
            'lecturerName' => 'required',
            'action' => 'required',
            'groupName' => 'required',
            'member1'    => 'required',

        ]);
        $practice = new Practice();
        $practice->course = $request->course;
        $practice->lecturerName = $request->lecturerName;
        $practice->action = $request->action;
        $practice->groupName = $request->groupName;
        $practice->member = $request->member1 . ', ' . $request->member2 . ", " . $request->member3. ", " . $request->member4. ", " . $request->member5;
        $practice->save();
        Alert::success('Success', 'Data Absensi Praktikum Success');
        return redirect(route('practice.create'));

    }

    public function fetch(Request $request)
    {
        $query = $request->get('query');
        if ($query) {
            $data = Client::where('name', 'LIKE', "%{$query}%")
                ->get();
            $output = '<ul class="dropdown-menu" style="display:block; position:absolute">';
            foreach ($data as $row) {
                $output .= '
                <li><a href="#">' . $row->name . '</a></li>
                ';
            }
            $output .= '</ul>';
            echo $output;
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Practice  $practice
     * @return \Illuminate\Http\Response
     */
    public function show(Practice $practice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Practice  $practice
     * @return \Illuminate\Http\Response
     */
    public function edit(Practice $practice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Practice  $practice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Practice $practice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Practice  $practice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Practice $practice)
    {
        $practice = Practice::find($practice->id);
        $practice->delete();
        return redirect(route('practice.index'));
    }
}
