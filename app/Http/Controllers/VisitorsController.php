<?php

namespace App\Http\Controllers;

use App\Models\Visitors;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class VisitorsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except('create', 'store');
    }
    public function index()
    {
        $visitors = Visitors::orderBy('created_at', 'desc')->get();
        return view('admin.attendance.visitors', compact('visitors'));
    }

    function fetch_data(Request $request)
    {
     if($request->ajax())
     {
      if($request->from_date != '' && $request->to_date != '')
      {
       $data = DB::table('visitors')
         ->whereBetween(DB::raw('DATE(created_at)'), array($request->from_date, $request->to_date))->get();
      }
      else
      {
       $data = DB::table('visitors')->orderBy('created_at', 'desc')->get();
      }
      echo json_encode($data);
     }
    }

    public function create()
    {
        return view('user.visitors');
    }

    public function store(Request $request)
    {

        // dd($request);
        $this->validate(request(), [
            'identityNum'    => 'required',
            'name'    => 'required',
        ]);
        // to question
        $visitors = new Visitors();
        $visitors->identityNum = $request->identityNum;
        $visitors->name = $request->name;
        $visitors->save();
        Alert::success('Success', 'Data Absensi Pengunjung Success');
        return redirect(route('visitor.create'));
    }

    public function show(Visitors $visitors)
    {
        //
    }

    public function edit(Visitors $visitors)
    {
        //
    }

    public function update(Request $request, Visitors $visitors)
    {
        //
    }

    public function destroy(Visitors $visitors)
    {
        $visitors = Visitors::find($visitors->id);
        $visitors->delete();
        return redirect(route('visitors.index'));
    }
}
