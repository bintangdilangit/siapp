<?php

namespace App\Http\Controllers;

use App\Models\Material;
use Illuminate\Http\Request;

class MaterialController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $materials = Material::orderBy('updated_at', 'desc')->get();
        // dd($materials);
        return view('admin.material.index', compact('materials'));
    }

    public function create()
    {
        return view('admin.material.create');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'kategori'    => 'required',
            'name'    => 'required',
            'desc'    => 'required',
            'stock'    => 'required',
        ]);
        $data = $request->kategori;
        unset($data[count($request->kategori)-1]);;
        foreach ($data as $key => $value) {
            $material[] = [
                'kategori' => $request->kategori[$key],
                'name' => $request->name[$key],
                'desc' => $request->desc[$key],
                'stock' => $request->stock[$key],
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }
        Material::insert($material);


        alert()->success('Data Alat Bahan Success');
        return redirect(route('material.index'));
    }

    public function show(Material $material)
    {
        //
    }

    public function edit(Material $material)
    {
        $material = Material::where('id', $material->id)->first();
        return view('admin.material.edit', compact('material'));
    }

    public function update(Request $request, $id)
    {
        $this->validate(request(), [
            'kategori'    => 'required',
            'name'    => 'required',
            'desc'    => 'required',
            'stock'    => 'required',
        ]);
        $material = Material::find($id);
        $material->kategori = $request['kategori'];
        $material->name = $request['name'];
        $material->desc = $request['desc'];
        $material->stock = $request['stock'];

        $material->save();
        return redirect('material');
    }

    public function destroy(Material $material)
    {
        $material = Material::find($material->id);
        $material->delete();
        return redirect(route('material.index'));
    }
}
