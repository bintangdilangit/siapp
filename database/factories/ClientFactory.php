<?php

namespace Database\Factories;

use App\Models\Client;
use App\Models\Role;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Client::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $roles = Role::pluck('id')->toArray();
        return [
            'noId' => $this->faker->randomNumber,
            'name' => $this->faker->name,
            'tlp' => $this->faker->phoneNumber,
            'address' => $this->faker->address,
            'role_id' => $this->faker->randomElement($roles),
        ];
    }
}
