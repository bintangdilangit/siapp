<?php

use App\Http\Controllers\BorrowController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\MaterialController;
use App\Http\Controllers\PracticeController;
use App\Http\Controllers\ReverseController;
use App\Http\Controllers\VisitorsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('user.index');
})->name('user');


Route::get('/dashboard',[DashboardController::class,'index'])->name('dashboard');

require __DIR__.'/auth.php';

Route::resource('/material',MaterialController::class);



Route::prefix('/client')->group(function (){
    Route::prefix('/mahasiswa')->group(function (){
        Route::get('/',[ClientController::class,'indexMahasiswa'])->name('client.mahasiswa.index');
        Route::get('/create',[ClientController::class,'createMahasiswa'])->name('client.mahasiswa.create');
        Route::post('/create',[ClientController::class,'storeMahasiswa'])->name('client.mahasiswa.store');
        Route::get('/{mahasiswa}/edit',[ClientController::class,'editMahasiswa'])->name('client.mahasiswa.edit');
        Route::put('/{mahasiswa}',[ClientController::class,'updateMahasiswa'])->name('client.mahasiswa.update');
        Route::delete('/{mahasiswa}',[ClientController::class,'destroyMahasiswa'])->name('client.mahasiswa.destroy');

    });
    Route::prefix('/dosen')->group(function (){
        Route::get('/',[ClientController::class,'indexDosen'])->name('client.dosen.index');
        Route::get('/create',[ClientController::class,'createDosen'])->name('client.dosen.create');
        Route::post('/create',[ClientController::class,'storeDosen'])->name('client.dosen.store');
        Route::get('/{dosen}/edit',[ClientController::class,'editDosen'])->name('client.dosen.edit');
        Route::put('/{dosen}',[ClientController::class,'updateDosen'])->name('client.dosen.update');
        Route::delete('/{dosen}',[ClientController::class,'destroyDosen'])->name('client.dosen.destroy');

    });
    Route::prefix('/instansi')->group(function (){
        Route::get('/',[ClientController::class,'indexInstansi'])->name('client.instansi.index');
        Route::get('/create',[ClientController::class,'createInstansi'])->name('client.instansi.create');
        Route::post('/create',[ClientController::class,'storeInstansi'])->name('client.instansi.store');
        Route::get('/{instansi}/edit',[ClientController::class,'editInstansi'])->name('client.instansi.edit');
        Route::put('/{instansi}',[ClientController::class,'updateInstansi'])->name('client.instansi.update');
        Route::delete('/{instansi}',[ClientController::class,'destroyInstansi'])->name('client.instansi.destroy');
    });

});
Route::prefix('/transaction')->group(function(){
    Route::resource('/borrow',BorrowController::class);
    Route::get('/borrow/edit/material/{material}/{borrow}',[BorrowController::class,'editBorrowMaterial'])->name('edit.borrow.material');
    Route::put('/borrow/edit/material/{material}/{borrow}',[BorrowController::class,'updateBorrowMaterial'])->name('update.borrow.material');
    Route::resource('/reverse',ReverseController::class);
    // Route::post('/reverse/{reverse}/approve',[ReverseController::class,'approve'])->name('wes');
    Route::post('/borrow/{borrow}/approve',[BorrowController::class,'approve'])->name('approve');
});
Route::prefix('/absensi')->group(function(){
    Route::resource('/practice',PracticeController::class);
    Route::resource('/visitor',VisitorsController::class);
    Route::post('/visitor/fetch_data', [VisitorsController::class,'fetch_data'])->name('visitor.fetch_data');
    Route::post('/visitor/create/fetch',[VisitorsController::class, 'fetch'])->name('visitor.create.fetch');
    Route::post('/practice/create/fetch',[PracticeController::class, 'fetch'])->name('practice.create.fetch');
});